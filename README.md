
# MSA Demo 프로젝트 입니다.
`Jhipster+Docker+GitLab CI` 를 활용해서 MSA 개발 구조를 10분이내에 준비 합니다.

## 제약사항

데모에서 사용된 소프트웨어의 버젼입니다. 설치방법 등은 생략합니다.
```
$ docker -v
Docker version 19.03.2, build 6a30dfc

$ docker-compose -v
docker-compose version 1.24.1, build 4667896b

$ node -v
v8.16.0

$ java -version
java version "1.8.0_172"
Java(TM) SE Runtime Environment (build 1.8.0_172-b11)
Java HotSpot(TM) 64-Bit Server VM (build 25.172-b11, mixed mode)

```

## JHipster 설치

JHipster 를 설치 합니다.
```
$ npm install generator-jhipster -g
```

설치 된 버젼을 확인 합니다.
```
$ jhipster --version
INFO! Using JHipster version installed globally
6.3.0
```

## JDL (Jhipster Domain Language) 파일 가져오기
샘플로 정리한 jdl 파일이 있는 레파지토리를 레파지토리에서 클론합니다.

```
$ git clone https://gitlab.com/GitLab-KR/workshop/201909-devops-kr/msa-sample.git msa-sample
```
JDL(JHipster Domain Language)을 통해 엔티티와 릴레이션, 그리고 심지어 어플리케이션에 대한 설정을 할 수 있습니다.
JDL 파일은 [JDL Studio](https://start.jhipster.tech/jdl-studio/)를 이용해서 UML로 확인 할 수 있습니다.

```
$ cat msa-sample/store.jdl
application {
  config {
    baseName store,
    applicationType gateway,
    packageName com.jhipster.demo.store,
    serviceDiscoveryType eureka,
    authenticationType jwt,
    prodDatabaseType mysql,
    cacheProvider hazelcast,
    buildTool gradle,
    languages [ko,en],
    clientFramework react,
    useSass true,
    testFrameworks [protractor]
  }
  entities *
}
application {
  config {
    baseName invoice,
    applicationType microservice,
    packageName com.jhipster.demo.invoice,
    serviceDiscoveryType eureka,
    authenticationType jwt,
    prodDatabaseType mysql,
    buildTool gradle,
    languages [kocd ,en],
    serverPort 8081,
    skipUserManagement true
  }
  entities Invoice, Shipment
}
application {
  config {
    baseName notification,
    applicationType microservice,
    packageName com.jhipster.demo.notification,
    serviceDiscoveryType eureka,
    authenticationType jwt,
    databaseType mongodb,
    prodDatabaseType mongodb,
    devDatabaseType mongodb,
    cacheProvider no,
    enableHibernateCache false,
    buildTool gradle,
    languages [ko,en],
    serverPort 8082,
    skipUserManagement true
  }
  entities Notification
}

/* Entities for Store Gateway */
/** Product sold by the Online store */
entity Product {
    name String required
    description String
    price BigDecimal required min(0)
    size Size required
    image ImageBlob
}
enum Size {
    S, M, L, XL, XXL
}
entity ProductCategory {
    name String required
    description String
}
entity Customer {
    firstName String required
    lastName String required
    gender Gender required
    email String required pattern(/^[^@\s]+@[^@\s]+\.[^@\s]+$/)
    phone String required
    addressLine1 String required
    addressLine2 String
    city String required
    country String required
}
enum Gender {
    MALE, FEMALE, OTHER
}
entity ProductOrder {
    placedDate Instant required
    status OrderStatus required
    code String required
    invoiceId Long
}
enum OrderStatus {
    COMPLETED, PENDING, CANCELLED
}
entity OrderItem {
    quantity Integer required min(0)
    totalPrice BigDecimal required min(0)
    status OrderItemStatus required
}
enum OrderItemStatus {
    AVAILABLE, OUT_OF_STOCK, BACK_ORDER
}
relationship OneToOne {
    Customer{user(login) required} to User
}
relationship ManyToOne {
 OrderItem{product(name) required} to Product
}
relationship OneToMany {
   Customer{order} to ProductOrder{customer(email) required},
   ProductOrder{orderItem} to OrderItem{order(code) required} ,
   ProductCategory{product} to Product{productCategory(name)}
}
service Product, ProductCategory, Customer, ProductOrder, OrderItem with serviceClass
paginate Product, Customer, ProductOrder, OrderItem with pagination
/* Entities for Invoice microservice */
entity Invoice {
    code String required
    date Instant required
    details String
    status InvoiceStatus required
    paymentMethod PaymentMethod required
    paymentDate Instant required
    paymentAmount BigDecimal required
}
enum InvoiceStatus {
    PAID, ISSUED, CANCELLED
}
entity Shipment {
    trackingCode String
    date Instant required
    details String
}
enum PaymentMethod {
    CREDIT_CARD, CASH_ON_DELIVERY, PAYPAL
}
relationship OneToMany {
    Invoice{shipment} to Shipment{invoice(code) required}
}
service Invoice, Shipment with serviceClass
paginate Invoice, Shipment with pagination
microservice Invoice, Shipment with invoice
/* Entities for notification microservice */
entity Notification {
    date Instant required
    details String
    sentDate Instant required
    format NotificationType required
    userId Long required
    productId Long required
}
enum NotificationType {
    EMAIL, SMS, PARCEL
}
microservice Notification with notification

```

## JDL import

jhipster 커맨드를 이용해서 생성한 store.jdl 파일 임포트
```
$ jhipster import-jdl store.jdl
```

실행 하면, **store**, **invoice**, **notification** 폴더가 생성되며, 다음 작업이 수행됩니다.
- 어플리케이션,엔티티,릴레이션 생성
- 설정에 따른 소스코드 생성
- NPM 의존 패키지 설치

```
...
...
...
Congratulations, JHipster execution is complete!
```

## docker-compose 설정 생성
임포트가 끝나면 로컬에 docker-compose를 이용해 마이크로서비스 개발 실행환경이 완료 되었고, 
docker-compose 를 설정하여 간단히 오케스트레이션 합니다.

```
$ mkdir docker-compose && cd docker-compose
$ jhipster docker-compose
```
docker-compose 폴더에 컴포즈 템플릿으로 생성되는 설정을 확인 합니다.


## docker-compose 로 구동해보기

docker-compose 로 구동하기 위한 이미지 생성이 필요하며 
각 **store**, **invoice**, **notification** 폴더에서 다음을 실행 합니다.
```
./gradlew -Pprod bootJar jibDockerBuild
```

생성된 docker image를 확인합니다.
```
docker images
REPOSITORY                                 TAG                 IMAGE ID            CREATED             SIZE
store                                      latest              449bf2fb6593        21 hours ago        310MB
notification                               latest              d47cc4616b97        22 hours ago        285MB
invoice                                    latest              b7ba13c2d522        22 hours ago        305MB
mysql                                      8.0.17              b8fd9553f1f0        6 days ago          445MB
```

이제 docker-compose 를 이용해서 구동합니다.
```
$ docker-compose up -d 
```

로그 확인을 합니다.
```
$ docker-compose logs -f
```

## 관리 UI 확인
- JHipster console (gateway) : http://localhost:8080
- JHipster Registry : http://localhost:8761
- JHipster Kibina console : http://localhost:5601


## GitLab CI/CD 파이프라인 생성 

각 마이크로서비스 어플리케이션별 ci/cd 를 위한 `.gitlab-ci.yml` 생성 하고, 추가로 gitlab registry 에 이미지를 푸시하기 위한 설정을 추가 합니다.

```
$ jhipster ci-cd
```

CI 설정 추가하기
```
docker-push:
    stage: release
    variables:
        REGISTRY_URL: $CI_REGISTRY
        IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
    dependencies:
        - gradle-package
    script:
        - ./gradlew jib -Djib.to.image="$IMAGE_TAG" -Djib.to.auth.username="$CI_REGISTRY_USER"  -Djib.to.auth.password="$CI_REGISTRY_PASSWORD"
```

